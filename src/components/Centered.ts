import styled from 'styled-components/native';

const Centered = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

export default Centered;
