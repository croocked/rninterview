function callAPIMiddleware({ dispatch, getState }) {
  return next => action => {
    const {
      types,
      callAPI,
      shouldCallAPI = () => true,
      payload = {},
      successCb,
      failureCb,
    } = action;

    if (!types) {
      return next(action);
    }

    if (
      !Array.isArray(types)
      || types.length !== 3
      || !types.every(type => typeof type === 'string')
    ) {
      throw new Error('Expected an array of three string types.');
    }

    if (typeof callAPI !== 'function') {
      throw new Error('Expected callAPI to be a function.');
    }

    if (!shouldCallAPI({ getState })) {
      return null;
    }

    const [requestType, successType, failureType] = types;

    dispatch({
      type: requestType,
      payload
    });

    return callAPI({ getState }).then(
      (response) => {
        dispatch({
          type: successType,
          payload: {
            ...payload,
            response
          }
        });
        if (successCb) {
          successCb({
            response, dispatch, getState
          });
        }
      },
      (error) => {
        dispatch({
          type: failureType,
          payload: {
            ...payload,
            error
          }
        });
        if (failureCb) {
          failureCb({
            error, dispatch, getState
          });
        }
      }
    );
  };
}

export default callAPIMiddleware;
