import React, { ReactNode } from 'react';
import styled from 'styled-components/native';
import Touchable from './Touchable';
import { Text } from 'react-native';
import { IUserPayload } from '../api/users';
import Avatar from './Avatar';

export interface IUsersListItemProps {
  onPress: () => void;
  user: IUserPayload;
}

export const ITEM_HEIGHT = 80;

const TouchableContainer = styled.View`
  width: 100%;
  height: ${ITEM_HEIGHT};
  padding: 16px;
  flex-direction: row;
  align-items: center;
`

const NameView = styled.View`
  margin-left: 16;
`

export const ItemSeparator = styled.View`
  width: 100%;
  height: 1;
  background-color: rgba(0, 0, 0, 0.4);
`

function UsersListItem({
  onPress,
  user
}) {
  return (
    <Touchable onPress={onPress}>
      <TouchableContainer>
        <Avatar source={{uri: user.avatar}} size={ITEM_HEIGHT*0.7}/>
        <NameView>
          <Text>{user.first_name}</Text>
          <Text>${user.email}</Text>
        </NameView>
      </TouchableContainer>
    </Touchable>
  )
}

export default UsersListItem;
