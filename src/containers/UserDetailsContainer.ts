import { useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getUserAction, createUserAction } from '../store/actions/usersActions';
import { withNavigation, NavigationScreenProps } from 'react-navigation';
import UserDetails from '../components/UserDetails';
import { IUserPayload } from '../api/users';

export interface UserDetailsContainerProps extends NavigationScreenProps {
  getUser: (id: string) => void;
  userDetails: object;
}

function UserDetailsContainer({
  navigation,
  getUser,
  userDetails
}: UserDetailsContainerProps) {
  const {
    userId
  } = navigation.state.params;

  useEffect(() => {
    getUser(userId);
  }, []);

  return UserDetails({
    userDetails
  })
}

const mapStateToProps = ({ users }) => ({
  userDetails: users.selectedUser
})

const mapDispatchToProps = (dispatch) => ({
  getUser: (id: string) => dispatch(getUserAction(id)),
  createUser: (data: IUserPayload) => dispatch(createUserAction(data))
})

export default compose(
  withNavigation,
  connect(mapStateToProps, mapDispatchToProps)
)(UserDetailsContainer);
