import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import middlewares from './middlewares';
import rootReducer from './reducers';

export default function initializeStore () {
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whiteList: ['users']
  };
  
  const pReducer = persistReducer(persistConfig, rootReducer);

  const store = createStore(
    pReducer,
    composeWithDevTools(applyMiddleware(...middlewares))
  )
  let persistor = persistStore(store)

  return {
    store,
    persistor
  }
}
