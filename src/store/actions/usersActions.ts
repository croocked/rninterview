import { getUsersList, getUser, createUser, IUserPayload } from "../../api/users"

export const GET_USERS_LIST_IN_PROGRESS = 'GET_USERS_LIST_IN_PROGRESS'
export const GET_USERS_LIST_SUCCESS = 'GET_USERS_LIST_SUCCESS'
export const GET_USERS_LIST_FAILURE = 'GET_USERS_LIST_FAILURE'

export function getUsersListAction(forceUpdate = false) {
  console.log('getUsersListAction forceUpdate', forceUpdate)
  return {
    types: [
      GET_USERS_LIST_IN_PROGRESS,
      GET_USERS_LIST_SUCCESS,
      GET_USERS_LIST_FAILURE
    ],
    callAPI: () => getUsersList(),
    shouldCallAPI: ({ getState }) => {
      const {
        users
      } = getState();
      return forceUpdate === true || users.data.length === 0;
    } 
  }
}

export const GET_USER_IN_PROGRESS = 'GET_USER_IN_PROGRESS'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
export const GET_USER_FAILURE = 'GET_USER_FAILURE'

export function getUserAction(id) {
  return {
    types: [
      GET_USER_IN_PROGRESS,
      GET_USER_SUCCESS,
      GET_USER_FAILURE
    ],
    callAPI: () => getUser(id),
    shouldCallAPI: ({ getState }) => {
      const {
        users
      } = getState();
      return users.selectedUser === null ||
        (users.selectedUser !== null && users.selectedUser.id !== id);
    } 
  }
}

export const CREATE_EMPLOYEE_IN_PROGRESS = 'CREATE_EMPLOYEE_IN_PROGRESS'
export const CREATE_EMPLOYEE_SUCCESS = 'CREATE_EMPLOYEE_SUCCESS'
export const CREATE_EMPLOYEE_FAILURE = 'CREATE_EMPLOYEE_FAILURE'

export function createUserAction(payload: IUserPayload) {
  return {
    types: [
      CREATE_EMPLOYEE_IN_PROGRESS,
      CREATE_EMPLOYEE_SUCCESS,
      CREATE_EMPLOYEE_FAILURE
    ],
    callAPI: () => createUser(payload),
    payload
  }
}