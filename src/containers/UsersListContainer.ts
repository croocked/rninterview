import { useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withNavigation, NavigationScreenProps } from 'react-navigation';
import { getUsersListAction } from '../store/actions/usersActions';
import UsersList from '../components/UsersList';
import { IUserPayload } from '../api/users';

export interface UsersListContainerProps extends NavigationScreenProps {
  getUsersList: (forceUpdate?: boolean) => void
  users: Array<IUserPayload>;
  refreshing: boolean;
}

function UsersListContainer({
  navigation: { navigate },
  users,
  getUsersList,
  refreshing
}: UsersListContainerProps) {
  useEffect(() => {
    getUsersList();
  }, []);

  const onItemClick = (userId: number) => {
    navigate('UserDetails', {
      userId
    })
  }

  const onRefresh = () => {
    console.log('refresh')
    getUsersList(true)
  }

  return UsersList({
    users,
    onItemClick,
    onRefresh,
    refreshing
  })
}

const mapStateToProps = ({ users }) => ({
  users: users.data,
  refreshing: users.status === 'in_progress'
})

const mapDispatchToProps = (dispatch) => ({
  getUsersList: (forceUpdate) => dispatch(getUsersListAction(forceUpdate))
})

export default compose(
  withNavigation,
  connect(mapStateToProps, mapDispatchToProps)
)(UsersListContainer);