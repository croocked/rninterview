import axios from 'axios';
import { buildApiUrl } from '../helpers';
import { AxiosPromise } from 'axios';

interface IUserPayload {
  id?: number;
  first_name: string;
  last_name: string;
  email: string;
  avatar?: string;
}
const usersEndpoint = '/users'

function getUsersList(): Promise<AxiosPromise> {
  return axios.get(buildApiUrl(usersEndpoint))
}

function getUser(id: string): Promise<AxiosPromise> {
  return axios.get(buildApiUrl(`${usersEndpoint}/${id}`))
}

function createUser(payload: IUserPayload): Promise<AxiosPromise> {
  return axios.post(buildApiUrl(usersEndpoint), payload)
}

function updateUser(id: string, payload: IUserPayload): Promise<AxiosPromise> {
  return axios.put(buildApiUrl(`${usersEndpoint}/${id}`), payload)
}

function deleteUser(id: string): Promise<AxiosPromise> {
  return axios.delete(buildApiUrl(`${usersEndpoint}/${id}`))
}

export {
  createUser,
  deleteUser as deleteUser,
  getUser as getUser,
  getUsersList as getUsersList,
  updateUser as updateUser,
  IUserPayload
};
