import React from 'react';
import { Text } from 'react-native';
import { useScreens } from 'react-native-screens';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import ApplicationNavigation from './navigation';
import initializeStore from './store';

useScreens();

function App() {
  const { store, persistor } = initializeStore();
  return (
    <Provider store={store}>
      <PersistGate loading={<Text>Loading...</Text>} persistor={persistor}>
        <ApplicationNavigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
