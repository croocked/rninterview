import React, { useState } from 'react';
import { Text } from 'react-native';
import Container from './Container';
import Button from './Button';
import CustomModal from './CustomModal';
import CreateUserFormContainer from '../containers/CreateUserFormContainer';

function CreateUserModal() {
  const [modalVisible, setModalVisible] = useState(false)
  const onFormSubmitted = () => {
    setModalVisible(false);
  }
  return (
      <CustomModal
        visible={modalVisible}
        onOpenModalBttnClick={setModalVisible.bind(null, !modalVisible)}
      >
        <Container>
          <CreateUserFormContainer onFormSubmitted={onFormSubmitted}/>
          <Button
            onPress={setModalVisible.bind(null, false)}
          >
            <Text>Exit</Text>
          </Button>
        </Container>
      </CustomModal>
  );
}

export default CreateUserModal;
