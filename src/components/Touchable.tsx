import React from 'react';
import { Platform } from 'react-native';
import styled, { css } from 'styled-components/native';
import {
  TouchableHighlight,
  TouchableNativeFeedback
} from "react-native-gesture-handler";

const touchableStyle = css`
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const StyledTouchableNativeFeedback = styled.TouchableNativeFeedback`
  ${touchableStyle}
`

const StyledTouchableHighlight = styled.TouchableOpacity`
  ${touchableStyle}
`

const Touchable = (props) => Platform.OS === 'android' ? <StyledTouchableNativeFeedback {...props}/> : <StyledTouchableHighlight {...props}/>;

export default Touchable;
