import usersReducer, { defaultUsersState } from '../usersReducer';
import {
  GET_USERS_LIST_IN_PROGRESS,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_FAILURE,
  GET_USER_IN_PROGRESS,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  CREATE_EMPLOYEE_IN_PROGRESS,
  CREATE_EMPLOYEE_SUCCESS,
  CREATE_EMPLOYEE_FAILURE
} from '../../actions/usersActions'

describe('users reducer', () => {
  it('should return the initial state', () => {
    expect(usersReducer(undefined, {})).toEqual(defaultUsersState)
  })

  it('should handle GET_USERS_LIST_IN_PROGRESS action type', () => {
    const action = {
      type: GET_USERS_LIST_IN_PROGRESS
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'in_progress'
    })
  })

  it('should handle GET_USERS_LIST_SUCCESS action type', () => {
    const action = {
      type: GET_USERS_LIST_SUCCESS,
      payload: { response: { data: { data: [{ first_name: 'Jan' }] } } }
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'success',
      data: [{ first_name: 'Jan' }]
    })
  })

  it('should handle GET_USERS_LIST_FAILURE action type', () => {
    const action = {
      type: GET_USERS_LIST_FAILURE,
      error: 'Test error'
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'failure',
      data: [],
      error: 'Test error'
    })
  })

  it('should handle GET_USER_IN_PROGRESS action type', () => {
    const action = {
      type: GET_USER_IN_PROGRESS
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'in_progress'
    })
  })

  it('should handle GET_USER_SUCCESS action type', () => {
    const action = {
      type: GET_USER_SUCCESS,
      payload: { response: { data: { data: { first_name: 'Jan' } } } }
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'success',
      selectedUser: { first_name: 'Jan' },
      error: null
    })
  })

  it('should handle GET_USER_FAILURE action type', () => {
    const action = {
      type: GET_USER_FAILURE,
      error: 'Test error'
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'failure',
      data: [],
      error: 'Test error'
    })
  })

  it('should handle CREATE_EMPLOYEE_IN_PROGRESS action type', () => {
    const action = {
      type: CREATE_EMPLOYEE_IN_PROGRESS
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'in_progress'
    })
  })

  it('should handle CREATE_EMPLOYEE_SUCCESS action type', () => {
    const action = {
      type: CREATE_EMPLOYEE_SUCCESS,
      payload: { response: { data: {
        id: '1',
        first_name: 'Jan',
        last_name: 'Kowalski',
        email: 'mail@mail.com'
      } } }
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'success',
      data: [
        {
          id: '1',
          first_name: 'Jan',
          last_name: 'Kowalski',
          email: 'mail@mail.com'
        }
      ],
      error: null
    })
  })

  it('should handle CREATE_EMPLOYEE_FAILURE action type', () => {
    const action = {
      type: CREATE_EMPLOYEE_FAILURE,
      error: 'Test error'
    }
    expect(
      usersReducer(defaultUsersState, action)
    ).toEqual({
      ...defaultUsersState,
      status: 'failure',
      error: 'Test error'
    })
  })
})