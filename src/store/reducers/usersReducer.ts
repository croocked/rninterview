import {
  GET_USERS_LIST_IN_PROGRESS,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_FAILURE,
  GET_USER_IN_PROGRESS,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  CREATE_EMPLOYEE_IN_PROGRESS,
  CREATE_EMPLOYEE_SUCCESS,
  CREATE_EMPLOYEE_FAILURE
} from '../actions/usersActions'
import { IUserPayload } from '../../api/users';

export interface IUsersReducerState {
  status: String;
  data: Array<IUserPayload>;
  error: any | null;
  selectedUser: IUserPayload | null;
}

export interface IUsersReducerAction {
  type?: string;
  payload?: any;
  error?: any;
}

export const defaultUsersState: IUsersReducerState = {
  status: 'start',
  data: [],
  error: null,
  selectedUser: null
};

function usersReducer(state: IUsersReducerState = defaultUsersState, action: IUsersReducerAction) {
  switch(action.type) {
    case GET_USERS_LIST_IN_PROGRESS:
      return {
        ...state,
        status: 'in_progress'
      };
    case GET_USERS_LIST_SUCCESS:
      return {
        ...state,
        error: null,
        status: 'success',
        data: action.payload.response.data.data
      };
    case GET_USERS_LIST_FAILURE:
      return {
        ...state,
        status: 'failure',
        error: action.error,
        data: []
      };
    case GET_USER_IN_PROGRESS:
      return {
        ...state,
        status: 'in_progress'
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        error: null,
        status: 'success',
        selectedUser: action.payload.response.data.data as IUserPayload
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        status: 'failure',
        error: action.error,
        selectedUser: null
      };
    case CREATE_EMPLOYEE_IN_PROGRESS:
      return {
        ...state,
        status: 'in_progress'
      };
    case CREATE_EMPLOYEE_SUCCESS:
      const responseData = action.payload.response.data as IUserPayload;
      return {
        ...state,
        error: null,
        status: 'success',
        data: [
          {
            id: responseData.id,
            first_name: responseData.first_name,
            last_name: responseData.last_name,
            email: responseData.email
          } as IUserPayload,
          ...state.data
        ]
      };
    case CREATE_EMPLOYEE_FAILURE:
      return {
        ...state,
        status: 'failure',
        error: action.error
      };
    default:
      return state;
  }
}

export default usersReducer