import React from 'react';
import Container from '../components/Container';
import UsersListContainer from '../containers/UsersListContainer';
import CreateUserModal from '../components/CreateUserModal';


function UsersScreen() {
  return (
    <Container>
      <UsersListContainer />
      <CreateUserModal/>
    </Container>
  )
}

export default UsersScreen;
