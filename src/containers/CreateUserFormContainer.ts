import { connect } from 'react-redux';
import { compose } from 'redux';
import { createUserAction } from '../store/actions/usersActions';
import { withNavigation, NavigationScreenProps } from 'react-navigation';
import { IUserPayload } from '../api/users';
import CreateUserForm from '../components/CreateUserForm';


const mapStateToProps = ({ users }) => ({
  userDetails: users.selectedUser
})

const mapDispatchToProps = (dispatch) => ({
  createUser: (data: IUserPayload) => dispatch(createUserAction(data))
})

export default compose(
  withNavigation,
  connect(mapStateToProps, mapDispatchToProps)
)(CreateUserForm);
