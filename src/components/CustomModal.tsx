import React, { useState } from 'react';
import styled from 'styled-components/native';
import Touchable from './Touchable';
import { Text, Modal } from 'react-native';
import Container from './Container';
import CreateUserForm from './CreateUserForm';
import Button from './Button';

const StyledTouchable = styled(Touchable)`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 70;
  width: 100%;
  justify-content: center;
  align-items: center;
  background: rgb(0, 145, 255);
`

const OpenModalBttnTitle = styled.Text`
  color: white;
  font-size: 18;
`

function OpenModalBttn({
  onPress
}) {
  return (
    <StyledTouchable onPress={onPress}>
        <OpenModalBttnTitle>
          + Add new user
        </OpenModalBttnTitle>
    </StyledTouchable>
  )
}

export interface ICustomModal {
  children: React.ReactNode;
  visible: boolean;
  onOpenModalBttnClick: any;
}

function CustomModal({
  visible,
  onOpenModalBttnClick,
  children
}: ICustomModal) {
  return (
    <>
      <OpenModalBttn onPress={onOpenModalBttnClick}/>
      <Modal
        animationType="slide"
        transparent={false}
        visible={visible}
      >
        {children}
      </Modal>
    </>
  );
}

export default CustomModal;
