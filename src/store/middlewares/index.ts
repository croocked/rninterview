import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import callAPIMiddleware from './callApiMiddleware';

const middlewares = [
  callAPIMiddleware,
  logger,
  ReduxThunk
]

export default middlewares;

