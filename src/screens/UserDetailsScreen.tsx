import React from 'react';
import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation';
import Container from '../components/Container';
import UserDetailsContainer from '../containers/UserDetailsContainer';

interface UserDetailsScreenProps {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>
}

function UserDetailsScreen({
  navigation
}: UserDetailsScreenProps) {
  return (
    <Container>
      <UserDetailsContainer />
    </Container>
  )
}

export default UserDetailsScreen;
