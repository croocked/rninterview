import React from 'react';
import { Text, FlatList } from 'react-native';
import styled from 'styled-components/native';

import UsersListItem, { ITEM_HEIGHT, ItemSeparator } from './UsersListItem';
import Centered from './Centered';
import CreateUserModal from './CreateUserModal';
import Container from './Container';
import { IUserPayload } from '../api/users';

interface UsersListProps {
  users: Array<IUserPayload>;
  onItemClick: (userId: number) => void;
  onRefresh: () => void;
  refreshing: boolean;
}

const StyledSafeAreaView = styled.SafeAreaView`
  flex: 1;
`

function UsersList({
  users,
  onItemClick,
  onRefresh,
  refreshing
}: UsersListProps) {
  const getItemLayout = (data: IUserPayload[], index: number) => ({
    length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index
  })

  return (
    <Container>
      <StyledSafeAreaView>
        <FlatList
          data={users}
          renderItem={({ item }) => <UsersListItem
            user={item}
            onPress={onItemClick.bind(null, item.id)}
          />}
          keyExtractor={item => `${item.id}`}
          getItemLayout={getItemLayout}
          ItemSeparatorComponent={ItemSeparator}
          ListEmptyComponent={(
            <Centered>
              <Text>No users fetched yet...</Text>
            </Centered>
          )}
          onRefresh={onRefresh}
          refreshing={refreshing}
        />
      </StyledSafeAreaView>
    </Container>
  )
}

export default UsersList;
