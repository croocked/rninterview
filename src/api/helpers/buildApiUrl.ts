import {
  API_URL
} from 'react-native-dotenv'

export default function buildApiUrl(endpoint: string): string {
  return `${API_URL}${endpoint}`
}