import React from 'react';
import styled from 'styled-components/native';

import Touchable from './Touchable';

const Button = styled(Touchable)`
  height: 70;
  width: 100%;
  justify-content: center;
  align-items: center;
  background: rgba(255, 255, 255, 1);
`

export default Button;
