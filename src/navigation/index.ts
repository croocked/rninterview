import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import UsersScreen from '../screens/UsersScreen';
import UserDetailsScreen from '../screens/UserDetailsScreen';

const SCREENS = {
  users: {
    screen: UsersScreen,
    title: 'List of users'
  },
  UserDetails: {
    screen: UserDetailsScreen,
    title: 'User details'
  }
}

const MainNavigator = createStackNavigator(SCREENS);

export default createAppContainer(MainNavigator);