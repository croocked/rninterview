import React, { useRef, useEffect, useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import t from 'tcomb-form-native';

import Button from './Button';
import { IUserPayload } from '../api/users';
import { NavigationScreenProps } from 'react-navigation';

export interface CreateUserFormProps extends NavigationScreenProps {
  createUser: (data: IUserPayload) => void;
  onFormSubmitted: () => void;
  initialValues?: IUserPayload;
}

export interface IFormData {
  first_name: string;
  last_name: string;
  email: string;
}

const Form = t.form.Form;

const UserFormConfig = t.struct({
  first_name: t.String,
  last_name: t.String,
  email: t.String
});

const UserFormOptions = {
  fields: {
    first_name: {
      error: 'required'
    },
    last_name: {
      error: 'required'
    },
    email: {
      error: 'required'
    }
  }
};


function CreateUserForm({
  createUser,
  onFormSubmitted,
  initialValues
}: CreateUserFormProps) {
  const form = useRef();
  const [formData, setFormData] = useState(initialValues);

  useEffect(() => {
    if (form && form.current) {
      form.current.getComponent('first_name').refs.input.focus();
    }
  }, [])

  const clearForm = () => {
    setFormData(null);
  }

  const handleSubmit = () => {
    const values = form.current.getValue() as IFormData;
    console.log('form value', values)
    createUser({
      first_name: values.first_name,
      last_name: values.last_name,
      email: values.email,
    });
    clearForm();
    onFormSubmitted && typeof onFormSubmitted === 'function' && onFormSubmitted();
  }
  return (
    <View style={styles.container}>
      <Form
        ref={form}
        type={UserFormConfig}
        options={UserFormOptions}
        value={formData}
      />
      <Button
        onPress={handleSubmit}
      >
        <Text>Sign Up!</Text>
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
});

export default CreateUserForm;
