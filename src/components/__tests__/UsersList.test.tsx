import React from 'react';
import { shallow } from 'enzyme';
import UsersList from '../UsersList';

describe('UsersList', () => {
    describe('Rendering', () => {
        it('should match to snapshot', () => {
            const component = shallow(<UsersList
              users={undefined}
              refreshing={false}
              onItemClick={() => { return }}
              onRefresh={() => { return }}
            />)
            expect(component).toMatchSnapshot()
        });
    });
});