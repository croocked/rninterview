import styled from 'styled-components/native';

const Avatar = styled.Image`
 ${props => `
    border-radius: ${props.size/2};
    width: ${props.size};
    height: ${props.size};
 `}
`

Avatar.defaultProps = {
  size: 200
}

export default Avatar;
