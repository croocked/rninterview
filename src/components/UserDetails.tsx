import React from 'react';
import styled from 'styled-components';
import Container from './Container';
import { Text } from 'react-native';
import Centered from './Centered';
import { IUserPayload } from '../api/users';
import Avatar from './Avatar';

interface UserDetailsProps {
  userDetails: IUserPayload;
}

function UserDetails({
  userDetails
}: UserDetailsProps) {
  if (!userDetails) return (
    <Centered>
      <Text>No user details...</Text>
    </Centered>
  )
  return userDetails && (
    <Container>
      <Avatar source={{uri: userDetails.avatar}} />
    </Container>
  )
}

export default UserDetails;
